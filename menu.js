//const { ClientRequest } = require("http");
const readline = require("readline");
const r1 = readline.createInterface(process.stdin, process.stdout);

var nhanvien = {
    user:[
        {taiKhoan: "em1", matKhau: "123"},
        {taiKhoan: "em2", matKhau: "1234"},
    ],
};

var khachHang = {
    customers:[
        {ten: "Nhật", CMND: "261678902"},
        {ten: "Phương", CMND: "234678123"},
        {ten: "Anh", CMND: "345654234"},
    ],
};

var phuKienMay = {
    RAM:[
        {ma: "SS-01", ten: "Samsung", dungLuongGB: "8", Bus_Speed: "3200", soLuong: 100, giaTien: 1000000},
        {ma: "M-01", ten: "MSI", dungLuongGB: "16", Bus_Speed: "2400", soLuong: 200, giaTien: 1500000},
        {ma: "K-01", ten: "Kingston", dungLuongGB: "8", Bus_Speed: "3000", soLuong: 250, giaTien: 1200000},
    ],
    CPU: [
        {ma: "IN-01", hang_CPU: "Intel", doRongThanhGhi: "64", Core: "i7", soLuong: 100, giaTien: 1300000},
        {ma: "AMD-01", hang_CPU: "AMD", doRongThanhGhi: "64", Core: "Ryzen7", soLuong: 500, giaTien: 800000},
        {ma: "IN-02", hang_CPU: "Intel", doRongThanhGhi: "32", Core: "i3", soLuong: 150, giaTien: 1000000},
        {ma: "IN-03", hang_CPU: "Intel", doRongThanhGhi: "32", Core: "i9", soLuong: 80, giaTien: 1100000}
    ],
    Key_Board: [
        {ma: "M-01", hangPhim: "MSI", loai: "Phím cơ", soLuong: 3000, giaTien: 1500000},
        {ma: "D-01", hangPhim: "DELL", loai: "Phím văn phòng", soLuong: 1000, giaTien: 1100000},
        {ma: "K-01", hangPhim: "Kingston", loai: "Phím cơ", soLuong: 1500, giaTien: 1800000},
        {ma: "AP-01", hangPhim: "Apple", loai: "Phím văn phòng", soLuong: 1300, giaTien: 500000},
    ],
    Chuot: [
        {ma: "RZ-01", hangChuot: "Razer", loai: "Chuột Gaming", soLuong: 2000, giaTien: 900000},
        {ma: "HP-01", hangChuot: "HP", loai: "Chuột văn phòng", soLuong: 2100, giaTien: 1000000},
        {ma: "D-01", hangChuot: "DELL", loai: "Chuột văn phòng", soLuong: 1000, giaTien: 1100000},
        {ma: "LT-01", hangChuot: "Logitech", loai: "Chuột Gaming", soLuong: 500, giaTien: 1200000},
    ]
};

var allInOne = {
    PC: [
        {maMay: "PC-01", tenMay: "A", hangSanXuat: "HP", soLuong: "1000"},
        {maMay: "PC-02", tenMay: "B", hangSanXuat: "DELL", soLuong: "1500"},
        {maMay: "PC-03", tenMay: "C", hangSanXuat: "MSI", soLuong: "2000"},
    ],
    Laptop: [
        {maMay: "LAP-01", tenMay: "X", hangSanXuat: "LENOVO", soLuong: "3000"},
        {maMay: "LAP-02", tenMay: "Y", hangSanXuat: "ASUS", soLuong: "2000"},
        {maMay: "LAP-03", tenMay: "Z", hangSanXuat: "RAZER", soLuong: "4000"},
    ],
};

var Custom = {
    PCCustom: [
        {maMay: "PCC-01", tenRAM: "Samsung", tenCPU: "Intel", tenKeyBoard: "MSI", tenChuot: "Razer", giaTien: 5000000},
        {maMay: "PCC-02", tenRAM: "MSI", tenCPU: "Intel", tenKeyBoard: "DELL", tenChuot: "HP", giaTien: 10000000},
        {maMay: "PCC-01", tenRAM: "Samsung", tenCPU: "AMD", tenKeyBoard: "Kingston", tenChuot: "DELL", giaTien: 15000000},
    ],
};
// "======================================= MENU CHÍNH ĐĂNG NHẬP, ĐĂNG KÝ THÀNH VIÊN QUẢN KHO ==========================================="
function LoginEmploy(){
    r1.question("Nhập tên tài khoản: ", (taiKhoan) => {
        r1.question("Nhập mật khẩu: ", (matKhau) => {
            for(var i = 0; i < nhanvien.user.length; i++){
                if(taiKhoan == nhanvien.user[i].taiKhoan && matKhau == nhanvien.user[i].matKhau)
                {
                    console.clear();
                    console.log("Đăng nhập thành công !");
                    chonYeuCau1();
                    break;
                }
                else
                {
                 console.clear();
                 console.log("Lỗi đăng nhập");
                 LoginEmploy();
                 break;
                 //process.exit();
                }
            }
        });
    });
}

function SignUpEmploy(){
    r1.question("Nhập tên tài khoản mới: ", (taiKhoanMoi) => {
        r1.question("Nhập mật khẩu: ", (matKhauMoi) => {
            for(var i = 0; i < nhanvien.user.length; i++){
                if(taiKhoanMoi == nhanvien.user[i].taiKhoan){
                    console.log("Tên tài khoản đã có người dùng, mời nhập lại !");
                    SignUpEmploy();
                    break;
                }
                else if(taiKhoanMoi != nhanvien.user[i].taiKhoan){
                    var add_em = nhanvien.user.push({taiKhoan: taiKhoanMoi, matKhau: matKhauMoi});
                    console.log("Bạn đã tạo thành công, mời đăng nhập !");
                    LoginEmploy();
                    break;
                }
            }
        });
    });
}
// "======================================= MENU CHỌN CÁC YÊU CẦU VỀ THÔNG TIN KHÁCH HÀNG ==========================================="
function chinhSuaThongTinKhach(){
    console.log("=======//=======");
    console.log("1. Quản lý thông tin khách hàng.");
    console.log("2. Chỉnh sửa thông tin khách hàng.");
    r1.question("Mời chọn yêu cầu: ", (cus) => {
        switch(cus){
            case "1":
                menuChinhSua(cus);
                break;
            case "2":
                DoiThongTinKhachHang();
                break;
        }
    });
}

function menuChinhSua(cus){
    switch(cus){
        case "1":
            console.clear();
            console.log("1. In danh sách khách hiện có.");
            console.log("2. Thêm thông tin khách hàng.");
            r1.question("Xin chọn mục cần làm: ", (chon) => {
                switch(chon){
                    case "1":
                        console.log(khachHang.customers);
                        //chinhSuaThongTinKhach();
                        break;
                    case "2":
                        themKhachHang();
                        break;
                }
            });
    }
}

function themKhachHang(){
    r1.question("Mời nhập tên khách hàng: ", (add_cusname) => {
        r1.question("Mời nhập số CMND: ", (add_cusid) => {
            for(var i = 0; i < khachHang.customers.length; i++){
                if(add_cusid == khachHang.customers[i].CMND){
                    console.clear();
                    r1.question("Thông tin khách hàng đã tồn tại, nhấn 0 để quay lại: ", (cus) => {
                        switch(cus){
                            case "0":
                                chinhSuaThongTinKhach();
                                break;
                        }
                    })
                    break;
                }
                else if(add_cusid != khachHang.customers[i].CMND){
                    khachHang.customers.push({ten: add_cusname, CMND: add_cusid});
                    console.clear();
                    console.log("Bạn đã thêm thành công !");
                    console.log(khachHang.customers);
                    
                    chinhSuaThongTinKhach();
                    break;
                }
            }
        });
    });
}

function menuQuanLyKhach(){
    console.log("1. Thông tin các khách hàng.");
    console.log("2. Đăng ký khách hàng mới.");
    r1.question("Mời chọn yêu cầu: ", (quanly) => {
        switch(quanly){
            case "1":
                chinhSuaThongTinKhach();
                break;
            case "2":
                themKhachHang();
                break;
            default:
                r1.question("Yêu cầu không tồn tại, mời nhập lại: ", (quanly) => {
                    menuQuanLyKhach();
                });
        }
    });
}

function DoiThongTinKhachHang(){
    console.clear();
    console.log(khachHang.customers);
    r1.question("Tên khách hàng cũ muốn sửa đổi: ", (oldName) => {
        r1.question("Số CMND khách hàng cũ muốn sửa đổi: ", (oldCMND) => {
    for(var i = 0; i < khachHang.customers.length; i++){
            if(oldCMND == khachHang.customers[i].CMND){
                r1.question("Tên khách hàng mới: ", (newName) => {
                    r1.question("Số CMND mới: ", (newCMND) => {
                        for(var j = 0; j < khachHang.customers.length; j++){
                            khachHang.customers[j].ten = newName;
                            khachHang.customers[j].CMND = newCMND;
                            console.log(khachHang.customers);
                            break;
                        }
                    });
                });
                break;
            }
            else{
                //console.log("loi");
                console.clear();
                continue;
            }
        };
    });
});
}
// "======================================= MENU CHỌN YÊU CẦU MUA BÁN LẮP RÁP MÁY TÍNH ==========================================="
function menuMayTinh(){
    console.clear();
    console.log("1. Thông tin máy lắp đặt sẵn.");
    console.log("2. Thông tin máy Custom.");
    console.log("3. Tính tổng tiền.");
    console.log("Nhấn 0 để quay lại !");
    r1.question("Mời nhập yêu cầu: ", (may) => {
        console.clear;
        chonMay(may);
    });
}

function chonMay(choice3){
    switch(choice3){
        case "1":
            console.clear;
            mayLapSan();
            break;
        case "2":
            console.clear;
            mayCustom();
            break;
        case "0":
            console.clear;
            chonYeuCau1();
            break;
        default:
            console.clear;
            console.log("Yêu cầu thực hiện chưa đúng, đang quay lại menu chính !");
            console.log("==========//==========");
            chonyeucau();
    }
}

function mayLapSan(){
    r1.question("Yêu cầu xem thông tin của PC hay Laptop ?: ", (maylapsan) => {
        switch(maylapsan){
            case "PC":
                console.log("========//========");
                console.log("1. In danh sách PC lắp sẵn.");
                console.log("2. Thêm thông tin PC lắp sẵn mới.");
                r1.question("Mời chọn yêu cầu: ", (pcallinone) => {
                    switch(pcallinone){
                        case "1":
                            pcAllInOne();
                            break;
                        case "2":
                            themPCAllInOne();
                            break;
                        case "0":
                            mayLapSan();
                    }
                });
                break;
            case "Laptop":
                console.log("========//========");
                console.log("1. In danh sách Laptop lắp sẵn.");
                console.log("2. Thêm thông tin Laptop lắp sẵn mới.");
                r1.question("Mời chọn yêu cầu: ", (pcallinone) => {
                    switch(pcallinone){
                        case "1":
                            lapAllInOne();
                            break;
                        case "2":
                            themLapAllInOne();
                            break;
                        case "0":
                            mayLapSan();
                    }
                });
                break;
            case "0":
                menuMayTinh();
                break;
        }
    })
}

function pcAllInOne(){
    console.log(allInOne.PC);
    console.log("===============//================");
    mayLapSan();
}

function themPCAllInOne(){
    r1.question("Nhập mã sản phẩm: ", (add_id_pc) => {
        r1.question("Nhập tên máy: ", (add_pc_name) => {
            r1.question("Nhập hãng sản xuất: ", (add_hang_pc) => {
                r1.question("Nhập số lượng máy: ", (add_pc_amount) => {
                    for(var i = 0; i < allInOne.PC.length; i++){
                        var addpc = allInOne.PC.push({maMay: add_id_pc, tenMay: add_pc_name, hangSanXuat: add_hang_pc, soLuong: add_pc_amount});
                        console.clear();
                        console.log(allInOne.PC);
                        mayLapSan();
                        break;
                    }
                });
            })
        })
    })
}

function lapAllInOne(){
    console.log(allInOne.Laptop);
    console.log("===============//================");
    mayLapSan();
}

function themPCAllInOne(){
    r1.question("Nhập mã sản phẩm: ", (add_id_lap) => {
        r1.question("Nhập tên máy: ", (add_lap_name) => {
            r1.question("Nhập hãng sản xuất: ", (add_hang_lap) => {
                r1.question("Nhập số lượng máy: ", (add_lap_amount) => {
                    for(var i = 0; i < allInOne.Laptop.length; i++){
                        var addlap = allInOne.Laptop.push({maMay: add_id_lap, tenMay: add_lap_name, hangSanXuat: add_hang_lap, soLuong: add_lap_amount});
                        console.clear();
                        console.log(allInOne.Laptop);
                        mayLapSan();
                        break;
                    }
                });
            })
        })
    })
}

function mayCustom(){
    console.log("1. In danh sách PC Custom đã có.");
    console.log("2. Đăng ký lắp ráp PC Custom mới.");
    r1.question("Mời chọn yêu cầu; ", (mayCus) => {
        switch(mayCus){
            case "1":
                console.clear();
                console.log(Custom.PCCustom);
                mayCustom();
                break;
            case "2":
                console.clear();
                DangKyMayCustom();
                break;
            case "0":
                mayLapSan();
                break;
        }
    })
}

function DangKyMayCustom(){
    r1.question("Nhập mã máy mới: ", (add_cus_id) => {
        r1.question("Mời lựa chọn RAM hãng: ", (add_cus_ram) => {
            const ramExist = phuKienMay.RAM.some(ramData => ramData.ten === add_cus_ram);
            function Amount_RAM(x){
                return x.ten === add_cus_ram;
            }
            var viTri = phuKienMay.RAM.findIndex(Amount_RAM);
            if(ramExist && phuKienMay.RAM[viTri].soLuong > 0){
                r1.question("Mời chọn CPU hãng: ", (add_cus_cpu) => {
                    const cpuExist = phuKienMay.CPU.some(cpuData => cpuData.hang_CPU === add_cus_cpu);
                    function Amount_CPU(y){
                        return y.hang_CPU === add_cus_cpu;
                    }
                    var viTri1 = phuKienMay.CPU.findIndex(Amount_CPU);
                    if(cpuExist && phuKienMay.CPU[viTri1].soLuong > 0){
                        r1.question("Mời chọn bàn phím hãng: ", (add_cus_keyboard) => {
                            const keyboardExist = phuKienMay.Key_Board.some(keyData => keyData.hangPhim === add_cus_keyboard);
                            function Amount_Key(z){
                                return z.hangPhim === add_cus_keyboard;
                            }
                            var viTri2 = phuKienMay.Key_Board.findIndex(Amount_Key);
                            if(keyboardExist && phuKienMay.Key_Board[viTri2].soLuong > 0){
                                r1.question("Mời chọn chuột hãng: ", (add_cus_mouse) => {
                                   const mouseExist = phuKienMay.Chuot.some(mouseData => mouseData.hangChuot === add_cus_mouse);
                                   function Amount_Mouse(t){
                                       return t.hangChuot === add_cus_mouse;
                                   }
                                   var viTri3 = phuKienMay.Chuot.findIndex(Amount_Mouse);
                                   if(mouseExist && phuKienMay.Chuot[viTri3].soLuong > 0){
                                       var add_PCCustom = Custom.PCCustom.push({maMay: add_cus_id, tenRAM: add_cus_ram, tenCPU: add_cus_cpu, tenKeyBoard: add_cus_keyboard, tenChuot: add_cus_mouse, tinhTien: phuKienMay.RAM[viTri].giaTien + phuKienMay.CPU[viTri1].giaTien + phuKienMay.Key_Board[viTri2].giaTien + phuKienMay.Chuot[viTri3].giaTien});

                                       mayCustom();
                                   }
                                   else{
                                       console.clear();
                                       console.log("Chuột bạn chọn đã hết hàng hoặc nhập chưa đúng tên");
                                       mayCustom();
                                   }
                                });
                            }
                            else{
                                console.clear();
                                console.log("Bàn phím bạn chọn đã hết hàng hoặc chưa đúng tên");
                                mayCustom();
                            }
                        });
                    }
                    else{
                        console.clear();
                        console.log("CPU bạn chọn đã hết hàng hoặc tên nhập chưa đúng !");
                        mayCustom();
                    }
                });
            }
            else{
                console.clear();
                console.log("RAM bạn chọn đã hết hàng hoặc tên nhập chưa đúng !");
                mayCustom();
            }
        });
    });
}

// ======================================== MENU PHỤ KIỆN MÁY TÍNH ==============================================
function menuPhukien1(){
    console.log("1. In danh sách phụ kiện đang có.");
    console.log("2. Chỉnh sửa thông tin phụ kiện.");
    console.log("Nhấn 0 để quay lại menu chính.");
    r1.question("Mời chọn yêu cầu: ", (pk) => {
        switch(pk){
            case "1":
                console.clear();
                console.log(phuKienMay);
                menuPhukien1();
                break;
            case "2":
                console.clear();
                menuChiTiet();
                break;
            case "0":
                console.clear();
                chonYeuCau1();
                break;
            default:
                errorHandler1();
                break;
        }
    });
}

function menuChiTiet(){
    console.log("1. Chỉnh sửa thông tin RAM");
    console.log("2. Chỉnh sửa thông tin CPU");
    console.log("3. Chỉnh sửa thông tin bàn phím");
    console.log("4. Chỉnh sửa thông tin chuột");
    console.log("Nhấn 0 để quay lại !");
    r1.question("Mời chọn yêu cầu: ", (pk1) => {
        switch(pk1){
            case "1":
                console.clear();
                menuRam();
                break;
            case "2":
                console.clear();
                menuCPU();
                break;
            case "3":
                console.clear();
                menuBanPhim();
                break;
            case "4":
                console.clear();
                menuChuot();
                break;
            case "0":
                console.clear();
                menuPhukien1();
                break;
            default:
                console.clear();
                errorHandler1();
                break;
        }
    });
}

function menuRam(){
    r1.question("Bạn muốn thêm, sửa giá hay in thông tin RAM (Ấn 0 để quay lại !) ? : ", (quest) => {
        switch(quest){
            case "Thêm":
                r1.question("Mời nhập mã RAM: ", (idRam) => {
                    const idRamExist = phuKienMay.RAM.some(idRAM => idRAM.ma === idRam);
                    if(idRamExist == false){
                        r1.question("Mời nhập tên hãng RAM: ", (nameRam) => {
                            r1.question("Mời nhập dung lượng RAM (GB):  ", (dlRam) => {
                                r1.question("Mời nhập BUS SPEED của RAM: ", (BusRam) => {
                                    r1.question("Mời nhập số lượng RAM: ", (slRam) => {
                                        r1.question("Mời nhập giá tiền: ", (tienRam) => {
                                            console.clear();
                                            phuKienMay.RAM.push({ma: idRam, ten: nameRam, dungLuongGB: dlRam, Bus_Speed: BusRam, soLuong: slRam, giaTien: tienRam});
                                            console.log(phuKienMay.RAM);
                                            menuRam();
                                        });
                                    });
                                });
                            });
                        });
                    }
                    else{
                        console.log("Mã bạn nhập đã tồn tại hoặc không đúng cú pháp !");
                        menuRam();
                    }
                });
                break;
            case "In":
                console.clear();
                console.log(phuKienMay.RAM);
                menuRam();
                break;
            case "Sửa":
                r1.question("Nhập mã RAM: ", (idRam1) => {
                    const idRam1Exist = phuKienMay.RAM.some(idRAM1 => idRAM1.ma === idRam1);
                    if(idRam1Exist){
                        r1.question("Nhập số lượng cần sửa: ", (slRamSua) => {
                            r1.question("Nhập giá tiền cần sửa", (tienRamSua) => {
                                function Amount_RAM(y){
                                    return y.ma === idRam1;
                                }
                                var viTri1 = phuKienMay.RAM.findIndex(Amount_RAM);
                                phuKienMay.RAM[viTri1].soLuong = slRamSua;
                                phuKienMay.RAM[viTri1].giaTien = tienRamSua;
                                console.log("Đã sửa thành công !");
                                menuRam();
                            });
                        });
                    }
                    else{
                        console.clear();
                        console.log("Không tìm thấy mã RAM !");
                        menuRam();
                    }
                })
                break;
            case "0":
                console.clear();
                menuChiTiet();
                break;
        }
    });
}
function menuCPU(){
    r1.question("Bạn muốn thêm hay in thông tin CPU (Ấn 0 để quay lại !) ? : ", (quest) => {
        switch(quest){
            case "Thêm":
                r1.question("Mời nhập mã CPU: ", (idCpu) => {
                    const idCpuExist = phuKienMay.CPU.some(idCPU => idCPU.ma === idCpu);
                    if(idCpuExist == false){
                        r1.question("Mời nhập tên hãng CPU: ", (nameCpu) => {
                            r1.question("Mời nhập độ rộng thanh ghi:  ", (drCpu) => {
                                r1.question("Mời nhập Core CPU: ", (CoreCpu) => {
                                    r1.question("Mời nhập số lượng CPU: ", (slCpu) => {
                                        r1.question("Mời nhập giá tiền: ", (tienCpu) => {
                                            console.clear();
                                            phuKienMay.CPU.push({ma: idCpu, ten: nameCpu, doRongThanhGhi: drCpu, Core: CoreCpu, soLuong: slCpu, giaTien: tienCpu});
                                            console.log(phuKienMay.CPU);
                                            menuRam();
                                        });
                                    });
                                });
                            });
                        });
                    }
                    else{
                        console.log("Mã bạn nhập đã tồn tại hoặc không đúng cú pháp !");
                        menuRam();
                    }
                });
                break;
                case "Sửa":
                    r1.question("Nhập mã CPU: ", (idCpu1) => {
                        const idCpu1Exist = phuKienMay.CPU.some(idCPU1 => idCPU1.ma === idCpu1);
                        if(idCpu1Exist){
                            r1.question("Nhập số lượng cần sửa: ", (slCpuSua) => {
                                r1.question("Nhập giá tiền cần sửa", (tienCpuSua) => {
                                    function Amount_CPU(y){
                                        return y.ma === idCpu1;
                                    }
                                    var viTri1 = phuKienMay.CPU.findIndex(Amount_CPU);
                                    phuKienMay.CPU[viTri1].soLuong = slCpuSua;
                                    phuKienMay.CPU[viTri1].giaTien = tienCpuSua;
                                    console.log("Đã sửa thành công !");
                                    menuCPU();
                                });
                            });
                        }
                        else{
                            console.clear();
                            console.log("Không tìm thấy mã CPU !");
                            menuCPU();
                        }
                    })
                    break;
            case "In":
                console.clear();
                console.log(phuKienMay.CPU);
                menuCPU();
                break;
            case "0":
                console.clear();
                menuChiTiet();
                break;
        }
    });
}

function menuBanPhim(){
    r1.question("Bạn muốn thêm hay in thông tin bàn phím ? (Ấn 0 để quay lại !) : ", (quest) => {
        switch(quest){
            case "Thêm":
                r1.question("Mời nhập mã bàn phím: ", (idKey) => {
                    const idKeyExist = phuKienMay.Key_Board.some(idKEY => idKEY.ma === idKey);
                    if(idKeyExist == false){
                        r1.question("Mời nhập tên hãng bàn phím: ", (nameKey) => {
                            r1.question("Mời nhập loại bàn phím: ", (lKey) => {
                                r1.question("Mời nhập số lượng bàn phím: ", (slKey) => {
                                    r1.question("Mời nhập giá tiền: ", (tienKey) => {
                                        console.clear();
                                        phuKienMay.Key_Board.push({ma: idKey, hangPhim: nameKey, loai: lKey, soLuong: slKey, giaTien: tienKey});
                                        console.log(phuKienMay.Key_Board);
                                        menuRam();
                                    });
                                });
                            });
                        });
                    }
                    else{
                        console.log("Mã bạn nhập đã tồn tại hoặc không đúng cú pháp !");
                        menuRam();
                    }
                });
                break;
                case "Sửa":
                    r1.question("Nhập mã bàn phím: ", (idKey1) => {
                        const idKey1Exist = phuKienMay.Key_Board.some(idKEY1 => idKEY1.ma === idKey1);
                        if(idKey1Exist){
                            r1.question("Nhập số lượng cần sửa: ", (slKeySua) => {
                                r1.question("Nhập giá tiền cần sửa", (tienKeySua) => {
                                    function Amount_KEY(y){
                                        return y.ma === idKey1;
                                    }
                                    var viTri1 = phuKienMay.Key_Board.findIndex(Amount_KEY);
                                    phuKienMay.Key_Board[viTri1].soLuong = slKeySua;
                                    phuKienMay.Key_Board[viTri1].giaTien = tienKeySua;
                                    console.log("Đã sửa thành công !");
                                    menuBanPhim();
                                });
                            });
                        }
                        else{
                            console.clear();
                            console.log("Không tìm thấy mã CPU !");
                            menuBanPhim();
                        }
                    })
                    break;
            case "In":
                console.clear();
                console.log(phuKienMay.Key_Board);
                menuBanPhim();
                break;
            case "0":
                console.clear();
                menuChiTiet();
                break;
        }
    });
}

function menuChuot(){
    r1.question("Bạn muốn thêm hay in thông tin chuột ? (Ấn 0 để quay lại !) : ", (quest) => {
        switch(quest){
            case "Thêm":
                r1.question("Mời nhập mã chuột: ", (idMouse) => {
                    const idMouseExist = phuKienMay.Chuot.some(idMOUSE => idMOUSE.ma === idMouse);
                    if(idMouseExist == false){
                        r1.question("Mời nhập tên hãng chuột: ", (nameMouse) => {
                            r1.question("Mời nhập loại chuột: ", (lMouse) => {
                                r1.question("Mời nhập số lượng chuột: ", (slMouse) => {
                                    r1.question("Mời nhập giá tiền: ", (tienMouse) => {
                                        console.clear();
                                        phuKienMay.Chuot.push({ma: idMouse, hangChuot: nameMouse, loai: lMouse, soLuong: slMouse, giaTien: tienMouse});
                                        console.log(phuKienMay.Chuot);
                                        menuRam();
                                    });
                                });
                            });
                        });
                    }
                    else{
                        console.log("Mã bạn nhập đã tồn tại hoặc không đúng cú pháp !");
                        menuRam();
                    }
                });
                break;
                case "Sửa":
                    r1.question("Nhập mã chuột: ", (id1) => {
                        const id1Exist = phuKienMay.Key_Board.some(idM1 => idM1.ma === id1);
                        if(id1Exist){
                            r1.question("Nhập số lượng cần sửa: ", (slMouseSua) => {
                                r1.question("Nhập giá tiền cần sửa", (tienMouseSua) => {
                                    function Amount_MOUSE(y){
                                        return y.ma === id1;
                                    }
                                    var viTri1 = phuKienMay.Chuot.findIndex(Amount_MOUSE);
                                    phuKienMay.Chuot[viTri1].soLuong = slMouseSua;
                                    phuKienMay.Chuot[viTri1].giaTien = tienMouseSua;
                                    console.log("Đã sửa thành công !");
                                    menuChuot();
                                });
                            });
                        }
                        else{
                            console.clear();
                            console.log("Không tìm thấy mã CPU !");
                            menuChuot();
                        }
                    })
                    break;
            case "In":
                console.clear();
                console.log(phuKienMay.Chuot);
                menuChuot();
                break;
            case "0":
                console.clear();
                menuChiTiet();
                break;
        }
    });
}
// "======================================= MENU CHÍNH CHỌN YÊU CẦU CHO THÀNH VIÊN QUẢN KHO - HÀM MAIN() ==========================================="
function chonYeuCau1(){
    console.log("==========//==========");
    console.log("1. Quản lý thông tin khách hàng.");
    console.log("2. Quản lý thông tin máy tính .");
    console.log("3. Kho phụ kiện máy tính.");
    console.log("Nhập 0 để thoát menu !");
    r1.question("Mời nhập yêu cầu: ", (a) => {
        console.clear;
        menuPhu(a);
    });
}

function menuPhu(chonyeucau1){
    switch(chonyeucau1){
        case "1":
            console.clear();
            menuQuanLyKhach();
            break;
        case "2":
            console.clear();
            menuMayTinh();
            break;
        case "3":
            console.clear();
            menuPhukien1();
            break;
        case "0":
            console.clear();
            chonYeuCau();
            break;
        default:
            errorHandler1();
    }
}

function errorHandler1(){
    r1.question("Yêu cầu không hợp lệ, mời nhập lại: ", (chonyeucau2) => {
        menu(chonyeucau2);
    });
}

function chonYeuCau(){
    console.log("1. Đăng nhập thành viên quản kho.");
    console.log("2. Đăng ký nhân viên mới.");
    console.log("3. Thông tin nhân viên.");
    console.log("Nhập 0 để thoát menu !");
    r1.question("Mời nhập yêu cầu: ", (chonyeucau) => {
        console.clear;
        menu(chonyeucau);
    });
}

function errorHandler(){
    r1.question("Yêu cầu không hợp lệ, mời nhập lại: ", (chonyeucau) => {
        menu(chonyeucau);
    });
}

function menu(choice){
    switch(choice){
        case "1":
            console.clear();
            LoginEmploy();
            break;
        case "2":
            console.clear();
            SignUpEmploy();
            break;
        case "3":
            console.clear();
            console.log(nhanvien.user);
            console.log("======//======");
            chonYeuCau();
            break;
        case "0":
            console.log("Hẹn gặp lại !");
            process.exit();
        default:
            errorHandler();
    }
}

function main() {
    chonYeuCau();
};
main();